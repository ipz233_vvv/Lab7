#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	float a;
	scanf("%f", &a);
	printf(a == 0 ? "Cannot divide by zero" : "%f", 1 / a);
	return 0;
}