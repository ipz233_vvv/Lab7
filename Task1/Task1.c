#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	float x, y;
	scanf("%f%f", &x, &y);
	float y1 = x + 1;
	float y2 = -x + 1;
	printf(y1 <= y && y2 <= y || y >= 0 && x < 0 && y <= y1 || y <= 0 && x > 0 && y <= y2 ? "true\n" : "false\n");
	return 0;
}