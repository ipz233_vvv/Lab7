#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int a;
	scanf("%d", &a);
	printf(a % 10 == a / 10 ? "true" : "false");
	return 0;
}